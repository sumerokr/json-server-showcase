const faker = require('faker')

module.exports = () => {
  // generate users
  const users = (new Array(500).fill(null))
    .map(() => ({
      id: faker.random.uuid(),
      name: faker.name.findName(),
      age: faker.random.number({
        min: 18,
        max: 60
      }),
      email: faker.internet.email(),
      gender: faker.random.arrayElement(['male', 'female']),
      jobTitle: faker.name.jobTitle()
    }))

  // generate departments
  const companies = (new Array(50).fill(null))
    .map(() => ({
      id: faker.random.uuid(),
      title: faker.company.companyName(),
      phone: faker.phone.phoneNumber(),
      address: {
        country: faker.address.country(),
        city: faker.address.city(),
        streetAddress: faker.address.streetAddress()
      }
    }))

  // set relations
  users.forEach(user => {
    user.companyId = faker.random.arrayElement(companies).id
  })

  // expose/reveal result
  return {
    users,
    companies
  }
}
